#!/bin/bash
#  this script sets the entrypoint for the TIDO that depends on the
#  branch it is build from.


echo CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}

case ${CI_COMMIT_REF_NAME} in
"main")
  sed -i "s ahikar-test\.sub ahikar.sub g" index.html
  echo "set entrypoint for production"
  ;;
"develop")
  sed -i "s ahikar-test\.sub ahikar-dev.sub g" index.html
  echo "set entrypoint for develop aka staging"
  ;;
*)
  echo "set entrypoint for testing"
  ;;
esac
