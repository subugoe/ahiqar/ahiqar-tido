# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.9.0 - 2021-05-12

### Fixed

- Added toggle and language key.

## 0.8.0 - 2021-04-19

### Added

- RTL display of the text. The RTL support of the viewer will soon be deprecated.

## 0.7.3 - 2021-04-13

### Fixed

- the project specific CSS is now provided in the build artifact.

## 0.7.2 - 2021-02-24

### Added

- Implementation of project specific stylings to the viewer.
- Added scss config.

## 0.7.1 - 2021-02-24

### Fixed

- bots are now prevented from trying to execute pipelines in the back end.
Bots attempting to will cause failing pipelines because they don't have access rights to the back end.

## 0.7.0 - 2020-11-02

### Added

- CI job receiving upstream updates of submodule TIDO

## 0.6.0 - 2020-10-19

### Fixed

- the previously added functionality broke on an update of the viewer application

## 0.5.0 - 2020-10-12

### Added

- include a static CSS resource to the viewer for project specific styling (mainly the text area)

## [0.4.0] - 2020-08-28

### Changed

- updated the TIDO (commit in the viewer's repo: 521f98fe)

## [0.3.0] - 2020-07-10

### Changed

- updated the TIDO (commit in the viewer's repo: 299ece9f)

## [0.2.0] - 2020-05-29

### Added

- this CHANGELOG
- issue and merge request implemented
