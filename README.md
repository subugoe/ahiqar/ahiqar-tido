# Ahiqar Frontend

This repository is a project specific customization of TIDO.
TIDO is integrated as git submodule.

The viewer can be reached at <https://ahikar.sub.uni.goettingen.de>.

It is integrated during the build of the backend, cf. <https://gitlab.gwdg.de/subugoe/ahiqar/backend#get-the-frontend>.

## Project Configuration

The project specific configuration of the viewer is done in `index.html`. This file replaces TIDO's default settings in `index.template.html`. The replacement is done in the CI.

## Text Styling

To add a specific styling of the text in accordance with the project's requirements a
specific stylesheet, `ahikar.css`, is available.
The file is going to be included in the build of the TIDO application.
For local development copy the two files like shown below.

```bash
cp --force index.html tido/src/index.template.html
cp --force ahikar.css tido/src/statics/support.css
```

You do not have to track the copied files in the version control system.

## Auto-update

### Upstream

The submodule TIDO will receive updates from its upstream project automatically.
The pipeline will be triggered by the upstream after a merge on TIDO's `main` branch and passes the necessary upstream commit SHA.
For convinence we MAY add a manual job trigger in the upstream repository.
The upstream currently is: [subugoe/emo/tido](https://gitlab.gwdg.de/subugoe/emo/tido).

### Downstream

Each time a commit is done on `develop`, this repository's CI triggers a new pipeline on
the back end repository's `develop` branch so that the changes in the viewer are visible
shortly after.
The same holds for this repo's `main` branch which triggers the pipelines of the back end
basing on its `main` branch.
